/**
 * This function must find the smallest n entries in an array, ordered from smallest to largest, including
 * repetitions. The function may not use any externally supplied library functions. In your answer
 * you should consider both performance and memory usage.
 *
 * For example, after running the following code:
 * @code{.c}
 * {
 *   const int inputs[] = {10, 2, 23, -4, 51};
 *   int outputs[2] = {0};
 *
 *   get_smallest_n_values(inputs, 5, outputs, 2);
 * }
 * @endcode
 *
 * outputs will contain the values {-4, 2}
 *
 * @param inputs Pointer to a constant array of numbers.
 * @param num_inputs Length of the inputs array.
 * @param outputs Pointer to a pre-allocated array into which to place the smallest
 *                n_outputs values from inputs.
 * @param n_outputs Number of values to place in outputs.
 */
void get_smallest_n_values(const int *inputs, int num_inputs, int *outputs, int n_outputs);


