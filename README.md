# C test

This directory contains the following files.

* `CMakeLists.txt`
* `get_best.c`
* `get_best.h`
* `README.md`
* `test_get_best.c` 


This is a cmake project; a valid `CMakeLists.txt` has been provided for you so you can build and run the test wrappers
to check your work.

## Test requirements

`get_best.c` contains a single function `get_smallest_n_values()`, which is currently empty. Full documentation for
the expected behaviour of this function is documented in `get_best.h`. Your task is to complete this function 
such that it passes the tests provided in `test_get_best.c`.

This function must find the smallest n entries in an array, ordered from smallest to largest including repetitions.  
The function may not use any externally supplied library functions. In your answer you should consider both 
performance and memory usage.

For example, after running the following code: 
```C
{
  const int inputs[] = {10, 2, 23, -4, 51};
  int outputs[2] = {0};

  get_smallest_n_values(inputs, 5, outputs, 2);
}
```

`outputs` will contain the values `{-4, 2}`.

## Build instructions

As a prerequisite, you should have access to a development system with `cmake` and a C compiler installed. 

```shell
interview_example$ mkdir build
interview_example$ cd build
interview_example/build$ cmake ..
-- The C compiler identification is GNU 9.3.0
-- Check for working C compiler: /usr/bin/cc
-- Check for working C compiler: /usr/bin/cc -- works
-- Detecting C compiler ABI info
-- Detecting C compiler ABI info - done
-- Detecting C compile features
-- Detecting C compile features - done
-- Configuring done
-- Generating done
-- Build files have been written to: interview_example/build
interview_example/build$ make
Scanning dependencies of target test_get_best
[ 33%] Building C object CMakeFiles/test_get_best.dir/test_get_best.c.o
[ 66%] Building C object CMakeFiles/test_get_best.dir/get_best.c.o
[100%] Linking C executable test_get_best
[100%] Built target test_get_best
```

## Execution instructions

As given, the build provided should produce a list passes and failures as follows.

```shell
interview_example/build$ ./test_get_best 
Running test1...FAIL
Running test2...FAIL
Running test3...FAIL
Running test4...PASS
Running test5...FAIL
Running test6...FAIL
5 tests failed
```

A correct implementation of `get_smallest_n_values()` should produce the output

```shell
interview_example/build$ ./test_get_best
Running test1...PASS
Running test2...PASS
Running test3...PASS
Running test4...PASS
Running test5...PASS
Running test6...PASS
Congratulations, all tests passed!
```
