#include <stdio.h>
#include <string.h>
#include <limits.h>
#include "get_best.h"

#define NUM_ELEMENTS(a) (sizeof(a)/sizeof(typeof(a[0])))

int test1() {
    const int inputs[] = {10, 2, 23, -4, 51};
    const int expected[] = {-4, 2};

    const int n_inputs = NUM_ELEMENTS(inputs);
    const int n_expected = NUM_ELEMENTS(expected);
    int outputs[n_expected];

    for (int i = 0; i < n_expected; i++) outputs[i] = INT_MIN;

    printf("Running %s...", __func__);
    get_smallest_n_values(inputs, n_inputs, outputs, n_expected);
    if (!memcmp(expected, outputs, sizeof(expected))) {
        printf("PASS\n");

        return 0;
    } else {
        printf("FAIL\n");
        return 1;
    }
}

int test2() {
    const int inputs[] = {10, 2, 23, -4, 51};
    const int expected[] = {-4, 2, 10, 23, 51};

    const int n_inputs = NUM_ELEMENTS(inputs);
    const int n_expected = NUM_ELEMENTS(expected);
    int outputs[n_expected];

    for (int i = 0; i < n_expected; i++) outputs[i] = INT_MIN;

    printf("Running %s...", __func__);
    get_smallest_n_values(inputs, n_inputs, outputs, n_expected);
    if (!memcmp(expected, outputs, sizeof(expected))) {
        printf("PASS\n");
        return 0;
    } else {
        printf("FAIL\n");
        return 1;
    }
}

int test3() {
    const int inputs[] = {10, 2, 23, 2, 51};
    const int expected[] = {2, 2};

    const int n_inputs = NUM_ELEMENTS(inputs);
    const int n_expected = NUM_ELEMENTS(expected);
    int outputs[n_expected];

    for (int i = 0; i < n_expected; i++) outputs[i] = INT_MIN;

    printf("Running %s...", __func__);
    get_smallest_n_values(inputs, n_inputs, outputs, n_expected);
    if (!memcmp(expected, outputs, sizeof(expected))) {
        printf("PASS\n");
        return 0;
    } else {
        printf("FAIL\n");
        return 1;
    }
}

int test4() {
    const int inputs[] = {10, 2, 23, 2, 51};

    const int n_inputs = NUM_ELEMENTS(inputs);
    const int n_expected = 0;
    int outputs[1]={INT_MAX};

    for (int i = 0; i < n_expected; i++) outputs[i] = INT_MIN;

    printf("Running %s...", __func__);
    get_smallest_n_values(inputs, n_inputs, outputs, n_expected);
    if (INT_MAX == outputs[0]) {
        printf("PASS\n");
        return 0;
    } else {
        printf("FAIL\n");
        return 1;
    }
}

int test5() {
    const int inputs[] = {0, 0, 0, 0, 0};
    const int expected[] = {0, 0};

    const int n_inputs = NUM_ELEMENTS(inputs);
    const int n_expected = NUM_ELEMENTS(expected);
    int outputs[n_expected];

    for (int i = 0; i < n_expected; i++) outputs[i] = INT_MIN;

    printf("Running %s...", __func__);
    get_smallest_n_values(inputs, n_inputs, outputs, n_expected);
    if (!memcmp(expected, outputs, sizeof(expected))) {
        printf("PASS\n");
        return 0;
    } else {
        printf("FAIL\n");
        return 1;
    }
}

int test6() {
    const int inputs[] = {1, 2, 3, 4, 5, -6, 7, 8, -9, 10, -1, -2, -3, -4, -5, -6, -7, -8, -9, 0};
    const int expected[] = {-9, -9, -8, -7, -6, -6, -5};

    const int n_inputs = NUM_ELEMENTS(inputs);
    const int n_expected = NUM_ELEMENTS(expected);
    int outputs[n_expected];

    for (int i = 0; i < n_expected; i++) outputs[i] = INT_MIN;

    printf("Running %s...", __func__);
    get_smallest_n_values(inputs, n_inputs, outputs, n_expected);
    if (!memcmp(expected, outputs, sizeof(expected))) {
        printf("PASS\n");
        return 0;
    } else {
        printf("FAIL\n");
        return 1;
    }
}

int main() {
    int failures = 0;
    failures+=test1();
    failures+=test2();
    failures+=test3();
    failures+=test4();
    failures+=test5();
    failures+=test6();
    if (failures){
        printf ("%d tests failed\n",failures);
    }
    else
    {
        printf("Congratulations, all tests passed!\n");
    }

}
